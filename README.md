# upload-artifact-registry

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag needs to be enabled to use the Component.

The `upload-artifact-registry` GitLab component copies Docker or [OCI][oci] images from the GitLab Container Registry to the [Google Cloud Artifact Registry][gar].

## Prerequisites

- Set up a Google Cloud workload identity pool and provider by following the steps in the [GitLab Google Cloud integration onboarding process][onboarding].
- Push a Docker image to your GitLab project's Container Registry.
- Create a Google Cloud project.
- Create an [Artifact Registry Docker repository][create-repo] to host your images.

## Billing

Usage of the `upload-artifact-registry` GitLab component might incur Google Cloud billing charges depending on your usage. For more information on Google Artifact Registry
pricing, see [Artifact Registry Pricing][pricing].

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/google-gitlab-components/artifact-registry/upload-artifact-registry@<VERSION>
    inputs:
      stage: deploy
      source: registry.gitlab.com/<GITLAB-GROUP>/<GITLAB-PROJECT>/<SOURCE-IMAGE:TAG>
      target: <LOCATION>-docker.pkg.dev/<GOOGLE-PROJECTID>/<REPOSITORY>/<TARGET-IMAGE:TAG>
```
- `<GITLAB-GROUP>` is the [GitLab Group][gitlab-group].
- `<GITLAB-PROJECT>` is the [GitLab Project][gitlab-project].
- `<VERSION>` is the release tag (e.g. `0.1.0`) or main of the `upload-artifact-registry` component.
- `<LOCATION> ` is the Artifact Registry [repository location][repository-location].
- `<GOOGLE-PROJECTID>` is the [Google Project ID][google-project].
- `<REPOSITORY>` is the Artifact Registry repository. For more information on repository naming in Artifact Registry, see [Repositories][repo].


The following example builds the image, stores it in the GitLab container registry, and pushes it to the Artifact Registry repository `us-central1-docker.pkg.dev/google-project/docker-repository` with the version `v1.0.0`.

```yaml
stages:
  - build
  - deploy

variables:
  GITLAB_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

build-sample-image:
  image: docker:24.0.5
  stage: build
  services:
    - docker:24.0.5-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $GITLAB_IMAGE .
    - docker push $GITLAB_IMAGE

include:
  - component: gitlab.com/google-gitlab-components/artifact-registry/upload-artifact-registry@0.1.0
    inputs:
      stage: deploy
      source: $GITLAB_IMAGE
      target: us-central1-docker.pkg.dev/google-project/docker-repository/image:v1.0.0
```

## Inputs

The following table lists inputs for the `upload-artifact-registry` component

| Input | Description | Example| Default value |
| ----- | ------- | ------------- | ----------- |
| `source` | (Required) GitLab Container Registry image path | `registry.gitlab.com/group/project/image:tag` | 	|
| `target` | (Required) Google Cloud Artifact Registry image path | `us-central1-docker.pkg.dev/project/repository/image:tag` |   |
| `as` | (Optional) The name of the job to be executed| `my-job` | `upload-artifact-registry` |
| `stage` | (Optional) The GitLab CI/CD stage | `my-stage`  | `deploy`  |

## Authorization

To use the `upload-artifact-registry` component, the workload identity pool must have the following minimum roles:

- Artifact Registry Writer ([`roles/artifactregistry.writer`][sa-ar-writer])
  - To access to read and write repository items

For example, to grant the `roles/artifactregistry.writer` role to all principals in a workload identity pool matching `developer_access=true` attribute mapping by the gcloud CLI:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/artifactregistry.writer"
```


[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[oci]: https://opencontainers.org/
[gar]: https://cloud.google.com/artifact-registry
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[gitlab-group]: https://docs.gitlab.com/ee/user/group/
[gitlab-project]: https://docs.gitlab.com/ee/user/project/index.html
[repository-location]: https://cloud.google.com/artifact-registry/docs/repositories/repo-locations
[google-project]:  https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects
[repo]: https://cloud.google.com/artifact-registry/docs/container-concepts#repositories
[create-repo]: https://cloud.google.com/artifact-registry/docs/repositories/create-repos#create
[sa-ar-writer]: https://cloud.google.com/iam/docs/understanding-roles#artifactregistry.writer
[pricing]: https://cloud.google.com/artifact-registry/pricing
